bfc:
	nasm -felf -o bfc.o bfc.asm
	ld -m elf_i386 -o bfc bfc.o
	rm bfc.o

bfc-debug:
	nasm -felf -o bfc-debug.o bfc-debug.asm
	ld -m elf_i386 -o bfc-debug bfc-debug.o
	rm bfc-debug.o
