section .text
  global _start

_start:
  push ebp
  mov ebp, esp

  xor eax, eax
  mov [loop_number], eax

  ; Save argv[1] in fn
  mov eax, [ebp+12]
  mov [fn], eax

  ; Open source file
  mov eax, 5
  mov ebx, [fn]
  mov ecx, 0
  mov edx, 0777
  int 80h

  ; Save fd
  mov [fd], eax

  ; Write initial code
  mov eax, 4
  mov ebx, [output]
  mov ecx, initial_code
  mov edx, initial_code_len
  int 80h

  read_loop:
    ; Read from file
    mov eax, 3
    mov ebx, [fd]
    mov ecx, data
    mov edx, 1
    int 80h

    cmp eax, 0 ; eof
    je end

    mov eax, 4
    mov ebx, [output]
    mov ecx, debug_data1
    mov edx, debug_data1_len
    int 80h

    mov eax, [data]
    call printNumber

    mov byte [char], 0xa
    call printChar

    mov eax, 4
    mov ebx, [output]
    mov ecx, debug_data
    mov edx, debug_data_len
    int 80h

    cmp byte [data], 0x2e ; .
    je print

    cmp byte [data], 0x2c ; ,
    je read

    cmp byte [data], 0x2b ; +
    je add_one

    cmp byte [data], 0x2d ; -
    je sub_one

    cmp byte [data], 0x3c ; <
    je left

    cmp byte [data], 0x3e ; >
    je right

    cmp byte [data], 0x5b ; [
    je open_loop

    cmp byte [data], 0x5d ; ]
    je close_loop

    jmp read_loop

print:
  mov eax, 4
  mov ebx, [output]
  mov ecx, print_data
  mov edx, print_data_len
  int 80h

  jmp read_loop

read:
  mov eax, 4
  mov ebx, [output]
  mov ecx, read_data
  mov edx, read_data_len
  int 80h

  jmp read_loop

add_one:
  mov eax, 4
  mov ebx, [output]
  mov ecx, add_one_data
  mov edx, add_one_data_len
  int 80h

  jmp read_loop

sub_one:
  mov eax, 4
  mov ebx, [output]
  mov ecx, sub_one_data
  mov edx, sub_one_data_len
  int 80h

  jmp read_loop

left:
  mov eax, 4
  mov ebx, [output]
  mov ecx, left_data
  mov edx, left_data_len
  int 80h

  jmp read_loop

right:
  mov eax, 4
  mov ebx, [output]
  mov ecx, right_data
  mov edx, right_data_len
  int 80h

  jmp read_loop

open_loop:
  inc dword [loop_number]
  push dword [loop_number]
  
  mov eax, 4
  mov ebx, [output]
  mov ecx, open_loop_data
  mov edx, open_loop_data_len
  int 80h

  mov eax, [loop_number]
  call printNumber
  mov byte [char], 0xa
  call printChar

  mov byte [char], "l"
  call printChar
  mov eax, [loop_number]
  call printNumber
  mov byte [char], ":"
  call printChar
  mov byte [char], 0xa
  call printChar

  jmp read_loop

close_loop:
  mov eax, 4
  mov ebx, [output]
  mov ecx, close_loop_data
  mov edx, close_loop_data_len
  int 80h

  pop eax
  call printNumber
  push eax
  mov dword [char], 0xa
  call printChar

  mov dword [char], "l"
  call printChar
  mov dword [char], "e"
  call printChar
  pop eax
  call printNumber
  mov dword [char], ":"
  call printChar
  mov dword [char], 0xa
  call printChar

  jmp read_loop

printNumber:
  push eax
  push edx
  xor edx, edx
  div dword [const10]
  cmp eax, 0
  je l1
  call printNumber
  l1:
  mov [char], edx
  add byte [char], '0'
  call printChar
  pop edx
  pop eax
  ret

printChar:
  push eax
  push edx
  mov eax, 4
  mov ebx, [output]
  mov ecx, char
  mov edx, 1
  int 80h
  pop edx
  pop eax
  ret

end:
  mov eax, 6
  mov ebx, [fd]
  int 80h

  mov eax, 4
  mov ebx, [output]
  mov ecx, end_data
  mov edx, end_data_len
  int 80h

  mov eax, 6
  mov ebx, [output]
  int 80h

  mov eax, 1
  xor ebx, ebx
  int 80h

section .data
  output dd 1
  initial_code db "section .data", 0xa, "array times 10240 db 0", 0xa, "section .bss", 0xa, "char resb 1", 0xa, "ptr resd 1", 0xa, "section .text", 0xa, "global _start", 0xa, "_start:", 0xa, "mov dword [ptr], array", 0xa
  initial_code_len equ $-initial_code
  print_data db "mov eax, 4", 0xa, "mov ebx, 1", 0xa, "mov ecx, [ptr]", 0xa, "mov edx, 1", 0xa, "int 80h", 0xa
  print_data_len equ $-print_data
  read_data db "mov eax, 3", 0xa, "mov ebx, 0", 0xa, "mov ecx, [ptr]", 0xa, "mov edx, 1", 0xa, "int 80h", 0xa
  read_data_len equ $-read_data
  add_one_data db "mov ecx, [ptr]", 0xa, "inc byte [ecx]", 0xa
  add_one_data_len equ $-add_one_data
  sub_one_data db "mov ecx, [ptr]", 0xa, "dec byte [ecx]", 0xa
  sub_one_data_len equ $-sub_one_data
  left_data db "dec dword [ptr]", 0xa
  left_data_len equ $-left_data
  right_data db "inc dword [ptr]", 0xa
  right_data_len equ $-right_data
  end_data db "mov eax, 1", 0xa, "xor ebx, ebx", 0xa, "int 80h", 0xa
  end_data_len equ $-end_data
  debug_data db "mov eax, 4", 0xa, "mov ebx, 1", 0xa, "mov ecx, char", 0xa, "mov edx, 1", 0xa, "int 80h", 0xa
  debug_data_len equ $-debug_data
  debug_data1 db "mov byte [char], "
  debug_data1_len equ $-debug_data1
  open_loop_data db "mov ecx, [ptr]", 0xa, "cmp byte [ecx], 0", 0xa, "je le"
  open_loop_data_len equ $-open_loop_data
  close_loop_data db "mov ecx, [ptr]", 0xa, "cmp byte [ecx], 0", 0xa, "jne l"
  close_loop_data_len equ $-close_loop_data
  const10 dd 10

section .bss
  fn resd 1
  fd resd 1
  data resb 1
  char resd 1
  loop_number resd 1
  num resd 1
