## Brainfuck Compiler

### Build
Build requirements: nasm  
`$ make`

### Usage
`$ ./bfc file_to_build.bf > output.asm`  
`$ nasm -felf output.asm -o output.o`  
`$ ld -m elf_i386 output.o -o output`
